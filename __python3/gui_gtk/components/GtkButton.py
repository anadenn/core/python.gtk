from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gio
#==========================================================

class GtkButton(GtkObject):
    
#==========================================================
    """

    """
    TEXT=String(default=None)
    ICON=String(default=None)

    

    #-------------------------------------------------
    def onBuildView(self):
        if self.icon is not None:
            view_object = Gtk.Button()
            icon = Gio.ThemedIcon(name=self.icon)
            image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
            view_object.add(image)
        else:
            view_object = Gtk.Button(self.text%self.data_object)

        return view_object
    #-------------------------------------------------

#==========================================================
