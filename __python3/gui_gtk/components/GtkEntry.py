from dataModel.models import *
from ..GtkObject import GtkObject
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#==========================================================

class GtkEntry(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.Entry
    #icon_name = "system-search-symbolic"
    ICON=String()
    TEXT=String()



    #-------------------------------------------------

    def onBuild(self,view_object,node):
        if self.text is not None:
            view_object.set_text(self.text%node)
        
        elif self.icon is not None:
            view_object.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, self.icon%node)        

    #-------------------------------------------------


#==========================================================
