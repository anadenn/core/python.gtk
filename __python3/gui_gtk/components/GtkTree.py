from dataModel.models import *
from core.program import *
from core.data import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,GLib,Gdk
import console
from gi.repository import GdkPixbuf
import os
#==========================================================

class GtkTreeLabel(Data):
    
#==========================================================

    TEXT=String()
    PYTYPE=Field(default=str,eval_expr=True)
    EDITABLE=Boolean(default=False)
    POSITION=Integer()
    HIDDEN=Boolean(default=False)

    def get_type(self):

        return self.pytype


    def get_data(self,node):

        return self.pytype(self.text%node)

    def get_render(self):
        self.renderer=self.onGetRender()
        if self.editable:
            self.renderer.set_property("editable", True)
            self.renderer.connect("edited", self.onTextEdited)

        return self.renderer

    def onGetRender(self):
        if self.pytype==bool:
            return Gtk.CellRendererToggle()
        else:
            return Gtk.CellRendererText()

    def onTextEdited(self, widget, path, text):
        self.parent.model[path][self.position] = text

#==========================================================

class GtkTreeIcon(GtkTreeLabel):
    
#==========================================================

    DEFAULT=String(default="Base.png")
    ICON_SIZE=Integer(default=16)

    def get_type(self):

        return GdkPixbuf.Pixbuf



    def get_data(self,node):

        name=self.get_icon_path(self.text%node)
        if not os.path.exists(name):
            name=self.get_icon_path(self.default)


        return GdkPixbuf.Pixbuf.new_from_file_at_size(name, self.icon_size, self.icon_size)


    def onGetRender(self):
        return Gtk.CellRendererPixbuf()

    def get_icon_path(self,path):

        if os.path.exists(path):
            return path

        path=os.path.join(os.environ["XMLPYTHON_PATH"],"__etc/gtk/icons",path)

        if os.path.exists(path):
            return path

 #==========================================================

class GtkTreeClassIcon(GtkTreeIcon):
    
#==========================================================

    def get_data(self,node):

        models=[]
        models.append(node.class_name)
        models.extend([cls.__name__+".png" for cls in node.__class__.models()])

        icon=None

        for elt in models:
            icon=self.get_icon_path(elt)
            if icon:
                break

        if icon is None:
            icon=self.get_icon_path(self.default)
    

        return GdkPixbuf.Pixbuf.new_from_file_at_size(icon, self.icon_size, self.icon_size)


#==========================================================

class GtkList(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.TreeView
    STORE_CLASS=Gtk.ListStore
    TEXT=String()
    #-------------------------------------------------
    def onBuild(self,view_object,node):

        self.node=node
        GtkTreeLabel(parent=self,name="__path",text="/%(tree_path)s",hidden=True)
        self.model=self.make_model()
        self.build_node(self.model,None,node)
        view_object.set_model(self.model)
        self.make_columns(view_object)        
        self.selection=view_object.get_selection()
        self.selection.connect("changed", self.onRowActivated)
        view_object.connect("key-press-event", self.onTreeNavigateKeyPress)


    #-----------------------------------------------------             
    def make_model(self):
        lst=[elt.get_type() for elt in self.children.by_class(GtkTreeLabel)]
        return self.__class__.STORE_CLASS(*lst)
    #-----------------------------------------------------             
    def make_columns(self,view_object):
        self.i=0
        for elt in self.children.by_class(GtkTreeLabel):
            elt.position=self.i
            renderer = elt.get_render()



            if isinstance(elt,GtkTreeIcon):
                column = Gtk.TreeViewColumn(elt.name, renderer, pixbuf=self.i)
            else:

                column = Gtk.TreeViewColumn(elt.name, renderer, text=self.i)


            if elt.hidden ==True:
                column.set_visible(False)

            #column.set_sort_column_id(i)
            view_object.append_column(column)
            self.i+=1

    #-----------------------------------------------------             
    def make_selection(self,node):
        for selector in self.children.by_class(Select):
            selector.set_selection([node,])
            for child in selector.get_selection():
                yield child


    #-----------------------------------------------------             
    def build_node(self,tree,tree_node,data_node):

        for child in self.make_selection(data_node):
            treenode=tree.append(self.make_data(data_node))


    #-----------------------------------------------------             
    def make_data(self,node,*args):
        r=list()
        for elt in self.children.by_class(GtkTreeLabel):
            r.append(elt.get_data(node))
        r.extend(args)
        return r

    #-------------------------------------------------
    def onRowActivated(self,selection,*args): 

        if self.text:
            model, treeiter = selection.get_selected()
            path=model[treeiter][self.i-1]
            elt=self.find(path)
            if elt is None:
                print(self.path()+" can't find selection ")
            else:
                #print("xxx",self.text%elt)
                result=self.call_view(self.text%elt,elt,self)
                self.event_message(elt=elt.path(),select=self.text,result=result)

    #-------------------------------------------------
    def onTreeNavigateKeyPress(self, treeview, event):
        keyname = Gdk.keyval_name(event.keyval)
        path, col = treeview.get_cursor()
        columns = [c for c in treeview.get_columns()] 
        colnum = columns.index(col)        

        if keyname == 'Tab':

            if colnum + 1 < len(columns):
                next_column = columns[colnum + 1]
            else: 
                next_column = columns[0]
            GLib.timeout_add(50,
                             treeview.set_cursor,
                             path, next_column, True)


        elif keyname == 'Return':

            model = treeview.get_model()
            #Check if currently in last row of Treeview
            if path.get_indices()[0] + 1 == len(model):
                path = treeview.get_path_at_pos(0,0)[0]
                #treeview.set_cursor(path, columns[colnum], True)
                GLib.timeout_add(50,
                             treeview.set_cursor,
                             path, columns[colnum], True)
            else:
                path.next()
                #treeview.set_cursor(path, columns[colnum], True)
                GLib.timeout_add(50,
                             treeview.set_cursor,
                             path, columns[colnum], True)
        else:
            pass

#==========================================================

class GtkTree(GtkList):
    
#==========================================================
    """

    """
    STORE_CLASS=Gtk.TreeStore


    #-----------------------------------------------------             
    def build_node(self,tree,tree_node,data_node):
        #print("xxx",data_node.path())

        for child in self.make_selection(data_node):
            #print(child.path())
            subnode=tree.append(tree_node,self.make_data(child))#(self.child.name,"/"+child.path())),"/"+child.path()
            self.build_node(tree,subnode,child)


#==========================================================
