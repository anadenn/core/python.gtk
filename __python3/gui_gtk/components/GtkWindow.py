from dataModel.models import *
from ..GtkObject import GtkObject
from dataModel.Store import Store
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from .GtkHeaderBar import GtkHeaderBar
#==========================================================

class GtkWindow(GtkObject):
    
#==========================================================
    """

    """

    W=Integer(default=900)
    H=Integer(default=900)
    MAXIMIZE=Boolean(default=True)


    #-------------------------------------------------        
    def onBuildView(self):
        view_object = Gtk.Window()

        if self.maximize:
            view_object.maximize() 
        else:
            view_object.set_default_size(self.w,self.h)

        view_object.set_border_width(20)
        view_object.connect("delete-event", self.onDestroyWindow)

        return view_object

    #-------------------------------------------------        
    def onDestroyWindow(self,*args):
        self.destroy()
        Gtk.main_quit()
    #-------------------------------------------------        
    def add(self,node):
        #print("#"*30,node)
        if  isinstance(node.view_object,GtkHeaderBar)==True:
            #print("view ok")
            self.view_object.set_titlebar(node.view_object)
        else:
            GtkObject.add(self,node)

    #-------------------------------------------------


#==========================================================
