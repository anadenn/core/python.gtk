from dataModel.models import *
from ..GtkObject import GtkObject
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#====================================================

class ListBoxRowWithData(Gtk.ListBoxRow):

#====================================================
    def __init__(self, data):
        super().__init__()
        self.data = data
        #self.add(Gtk.Label(label=data))
#==========================================================

class GtkList(GtkObject):
    
#==========================================================
    """

    """
    VIEW_CLASS=Gtk.ListBox
    SHOW_ON=String(default="/")



    #-------------------------------------------------

    def onBuild(self,view_object,node):
        view_object.connect("row-activated",self.onRowActivated)
        
    #-------------------------------------------------        
    def add(self,node):
        if isinstance(node,Gtk_Label):
            group=ListBoxRowWithData(node)
        else:
            group=Gtk.ListBoxRow()
        group.add(node.view_object)
        self.view_object.add(group)
    #-------------------------------------------------
    def onRowActivated(self,lst,row,*args): 
    
        if isinstance(row,ListBoxRowWithData):
            #print(args)       
            print("click on","/"+row.data.node.path())    
            self.win.make(row.data.node) 
            self.root.view_object.show_all()               
    #-------------------------------------------------


#==========================================================
